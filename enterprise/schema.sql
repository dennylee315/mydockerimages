

create database phone;

use phone;

create table numbers (
  name varchar(50),
  number varchar(30)
);

insert into numbers values('Steve Shilling', '555-111-2222');
insert into numbers values('Will', '555-888-5555');
insert into numbers value('Augie', "666-1111");
